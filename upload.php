<?php
$valid_file = false;
$message    = 'Something is happening..';

if( isset($_FILES['file']) && $_FILES['file']['name']) {
	//if no errors...
	if(!$_FILES['file']['error']) {
		//now is the time to modify the future file name and validate the file
		$newFileName = 'tracks/'.time().'.'.pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		if($_FILES['file']['size'] > (10240000)) //can't be larger than 10 MB
		{
			$message = 'Oops!  Your file\'s size is to large.';
		} else {
            $valid_file = true;
        }
		
		//if the file has passed the test
		if($valid_file) {
			//move it to where we want it to be
			move_uploaded_file($_FILES['file']['tmp_name'], $newFileName);
			$message = 'Congratulations!  Your file was accepted.';
		}
	}
	//if there is an error..
	else {
		//set that to be the returned message
		$message = 'Ooops!  Your upload triggered the following error:  '.$_FILES['photo']['error'];
	}
} else {
    // Show upload form
?>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
        <link rel="stylesheet" href="css/app.css"/>
        <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
    </head>
    <body>

    <?php include('menu.php'); ?>

    <section class="body">
        <h2>Upload GPX File</h2>
        <form enctype="multipart/form-data" action="upload.php" method="post">
            <div class="row">
                <div class="small-12 large-12 columns">
                    <input type="file" name="file" />
                </div>
                <div class="small-12 large-12 columns">
                    <input type="submit" class="button small" value="Upload" />
                </div>
            </div>
        </form>
    </section>
    </body>
    </html>
<?php
    die;
}

if (!$valid_file) {
    die($message);
}

include_once('library/track.class.php');

$travel = new Travel($newFileName);
$tracks = $travel->extract();
$statistics = new TravelStatistic($travel->tracks);
$firstPoint = $travel->tracks[0]->segments[0];

// Serialize
file_put_contents('serialized/'.md5($newFileName).'.ser', serialize($tracks));

?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>
    <script src="foundation-5.2.2/js/vendor/jquery.js"></script>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
    <script src="foundation-5.2.2/js/foundation.min.js"></script>
    
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>    
    <script src="js/gmap.cluster.time.js"></script>
    
    <script>
        function initialize() {
            var mapOptions = {
                zoom: 13,
                center: new google.maps.LatLng( <?=$firstPoint->latitude.','.$firstPoint->longitude;?> ),
                mapTypeId: google.maps.MapTypeId.MAP
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var travelCoordinates = [ 
                <?php
                $googleMapLatLngs = [];
                foreach($travel->getPoints() as $latLon) {
                    $googleMapLatLngs[] = 'new google.maps.LatLng('.$latLon->latitude.','.$latLon->longitude.')';
                }
                echo join(',', $googleMapLatLngs);
                ?>
            ];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < travelCoordinates.length; i++) {
                bounds.extend(travelCoordinates[i]);
            }
            map.fitBounds(bounds);
            var flightPath = new google.maps.Polyline({
                path: travelCoordinates,
                geodesic: true,
                strokeColor: '#FF6961',
                strokeOpacity: 0.8,
                strokeWeight: 2
            });
            flightPath.setMap(map);
            
            // Markers
            var unclusteredMarkers = [];
            <?php foreach($travel->getPoints() as $point) { ?>
            unclusteredMarkers.push({ 
                latitude:  <?=$point->latitude;?>, 
                longitude: <?=$point->longitude;?>, 
                timestamp: <?=$point->timestamp;?>
            });
            <?php } ?>
            
            var clusteredMarkers = cluster(unclusteredMarkers, 5);
            for (var i=0; i<clusteredMarkers.length; i++) {
                 var marker = new google.maps.Marker ({
                    position: new google.maps.LatLng(clusteredMarkers[i].latitude, clusteredMarkers[i].longitude),
                    title: 'Point #'+i,
                    map: map,
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: 4,
                        strokeWeight: 0,
                        fillOpacity: 1,
                        fillColor: '#B19CD9'
                    }
                });
            }            
        }
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
</head>
<body>
    <?php include('menu.php'); ?>

    <div id='map-canvas'></div>

    <section class="body">
        <div class="row">
            <div class="large-6 columns">
                <p>This marker are clustered from time based criteria. Other criteria is distance based.</p>
            </div>
        </div>

        <div class="row">
            <div class="large-6 columns">
                <h2>Your file detail</h2>
                <table>
                    <tr>
                        <th>Filename</th>
                        <td>:</td>
                        <td><?=$_FILES['file']['name'];?></td>
                    </tr>
                    <tr>
                        <th>Size</th>
                        <td>:</td>
                        <td><?=$_FILES['file']['size'];?> bytes</td>
                    </tr>
                    <tr>
                        <th>Filename on our server</th>
                        <td>:</td>
                        <td><?=$newFileName;?></td>
                    </tr>
                </table>
            </div>
            <div class="large-6 columns">
                <h2>Travel Statistics</h2>
                <table>
                    <tr>
                        <th>Travel Date</th>
                        <td>:</td>
                        <td><?=date("Y-m-d", $travel->tracks[0]->date);?></td>
                    </tr>
                    <tr>
                        <th>Total Point</th>
                        <td>:</td>
                        <td><?=$statistics->countPoint();?></td>
                    </tr>
                    <tr>
                        <th>Total Time Spent</th>
                        <td>:</td>
                        <td><?=$statistics->totalTime();?> s</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="large-6 columns">
                <a class="button expand" href="proceed.php?session=<?=md5($newFileName);?>">Save</a>
            </div>
            <div class="large-6 columns">
                <a class="button expand" href="upload.html">Cancel</a>
            </div>
        </div>
    </section>

</body>
</html>