<?php

include_once('library/database.php');
if (isset($_GET['id']) && $_GET['id'] != ""){
    $id = $_GET['id'];
} else {
    die("It's something..");
}

if (isset($_GET['mode']) && $_GET['mode'] != "") {
    $mode      = $_GET['mode'];
    $threshold = $_GET['threshold'];
} else {
    $mode  = 'time';
    $threshold = 10; // minutes
}

$track  = DB::queryFirstRow('SELECT * FROM tracks WHERE id=%i', $id);
$points = DB::query('SELECT * FROM points WHERE track_id=%i', $id);
$totalTime = DB::queryOneField('total_time', 'SELECT (MAX(timestamp) - MIN(timestamp)) as total_time FROM points WHERE track_id=%i', $id);
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>

    <script src="foundation-5.2.2/js/vendor/jquery.js"></script>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
    <script src="foundation-5.2.2/js/foundation.min.js"></script>
    <script src="js/jquery.gmap.tooltip.js"></script>

    <script src="js/string.format.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

    <style>
        #map-tooltip {
            height: 23px;
            font-size: 12px;
            color: white;
            padding: 5px;
            font-weight: bold;
            min-width: 150px;
        }

        #map-tooltip, .button {
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
        }

        #map-tooltip {
            background-color: #101010;
        }

        #tail {
            width: 0px;
            height: 0px;
            border: 5px solid;
            border-color: transparent #101010 transparent transparent;
            position:absolute;
            top: 7px;
            left: -10px;
        }
    </style>
    <?php if($mode == 'distance') { ?>
        <script src="js/gmap.cluster.distance.js"></script>
    <?php } else { ?>
        <script src="js/gmap.cluster.time.js"></script>
    <?php } ?>

    <script>
        function initialize() {
            var points = [
            <?php foreach($points as $point) { ?>
            { latitude: <?=$point['latitude'];?>, longitude: <?=$point['longitude'];?>, timestamp: <?=strtotime($point['timestamp']);?> },
            <?php } ?>
            ];
            var mapOptions = {
                center: new google.maps.LatLng( points[0]['latitude'], points[0]['longitude'] ),
                mapTypeId: google.maps.MapTypeId.MAP
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var travelCoordinates = [];
            for(var i=0; i<points.length; i++) {
                travelCoordinates.push(new google.maps.LatLng(points[i].latitude, points[i].longitude));
            }
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < travelCoordinates.length; i++) {
                bounds.extend(travelCoordinates[i]);
            }
            map.fitBounds(bounds);

            // Markers
            var clusteredMarkers = cluster(points, <?=$threshold;?>);
            //console.log(clusteredMarkers);
            var startTimestamp = clusteredMarkers[0].timestamp,
                markers = [];

            var before_show = function(marker, tooltip) {
                tooltip.html(marker.metadata.tooltip + '<div id="tail"></div>');

                marker.setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 5,
                    strokeWeight: 0,
                    fillOpacity: 1,
                    fillColor: '#F2555C'
                });
            };

            var before_hide = function(marker, tooltip) {
                marker.setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 4,
                    strokeWeight: 0,
                    fillOpacity: 1,
                    fillColor: '#B19CD9'
                });
            };

            // Attach tooltip to the DIV element
            var tooltip = new MapTooltip(map, $('#map-tooltip'), before_show, before_hide);

            for (var i=0; i<clusteredMarkers.length; i++) {
                var currentTimestamp = clusteredMarkers[i].timestamp,
                    currentTime      = moment.unix(currentTimestamp).format("h:mm:ss"),
                    duration         = moment.duration(currentTimestamp - startTimestamp, 'seconds'),
                    elapsed          = "{0}:{1}:{2}".format(duration.hours(), duration.minutes(), duration.seconds()),
                    markerTitle      = "{0} ({1} elapsed)".format(currentTime, elapsed);

                var marker = createMarker(clusteredMarkers[i].latitude, clusteredMarkers[i].longitude, markerTitle, map);
                markers.push(marker);
                tooltip.addMarker(marker); // markers are usually loaded from a database
            }

            function createMarker(latitude, longitude, title, map) {
                var marker = new google.maps.Marker ({
                        position: new google.maps.LatLng(latitude, longitude),
                        metadata : {
                            tooltip : title
                        },
                        map: map,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 4,
                            strokeWeight: 0,
                            fillOpacity: 1,
                            fillColor: '#B19CD9'
                        }
                    });

                return marker;
            }


            <?php if ($mode == 'time') { ?>
            var colors = ['#fc8d59','#ffffbf','#91cf60'];
            var flightPaths = [];
            cursor = 0;
            while(cursor < (clusteredMarkers.length-1)) {
                var lowerBand = clusteredMarkers[cursor].index;
                var upperBand = clusteredMarkers[cursor+1].index;

                for(var i=lowerBand; i<(upperBand-1); i++) {
                    flightPaths[i] = new google.maps.Polyline({
                        path: [travelCoordinates[i], travelCoordinates[i+1]],
                        geodesic: true,
                        strokeColor: colors[clusteredMarkers[cursor+1].cluster],
                        strokeOpacity: 0.8,
                        strokeWeight: 2
                    });
                    flightPaths[i].setMap(map);
                }
                //console.log('color: '+colors[clusteredMarkers[cursor+1].cluster])
                cursor++;
            }

            <?php } else { ?>
            var flightPath = new google.maps.Polyline({
                path: travelCoordinates,
                geodesic: true,
                strokeColor: '#FF6961',
                strokeOpacity: 0.8,
                strokeWeight: 2
            });
            flightPath.setMap(map);
            <?php }?>
        }
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
</head>
<body style="position: relative;">

<?php include('menu.php'); ?>

<div id='map-canvas'></div>
<div style="display:none; position: absolute;" id="map-tooltip"></div>

<section class="body">
    <br/>
    <form method="get" action="view.php">
        <input type="hidden" name="id" value="<?=$id;?>"/>
        <div class="row">
            <div class="small-8">
                <div class="row">
                    <div class="small-3 columns">
                        <label for="right-label" class="right inline">Cluster Criteria</label>
                    </div>
                    <div class="small-9 columns">
                        <select name="mode">
                            <option value="time">Time based (minutes)</option>
                            <option value="distance">Distance based (km)</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="right-label" class="right inline">Threshold</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" name="threshold" placeholder="10" value="10">
                    </div>
                </div>
                <div class="row">
                    <div class="small-offset-3 small-3">
                        <input type="submit" class="button tiny" value="submit"/>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="large-6 columns">
            <h2>Travel Statistics</h2>
            <table>
                <tr>
                    <th>Track name</th>
                    <td>:</td>
                    <td><?=$track['name'];?></td>
                </tr>
                <tr>
                    <th>Travel Date</th>
                    <td>:</td>
                    <td><?=$track['timestamp'];?></td>
                </tr>
                <tr>
                    <th>Total Point</th>
                    <td>:</td>
                    <td><?=count($points);?></td>
                </tr>
                <tr>
                    <th>Total Time Spent</th>
                    <td>:</td>
                    <td><?=gmdate("H",$totalTime);?> hours <?=gmdate("i",$totalTime);?> minutes <?=gmdate("s",$totalTime);?> seconds</td>
                </tr>
            </table>
        </div>
    </div>
</section>

</body>
</html>