<?php
include_once('library/track.class.php');
include_once('library/database.php');

ob_implicit_flush(true);

$processed = [];
foreach (new DirectoryIterator('batch') as $fileinfo) {
    if (!$fileinfo->isDot() && strtolower($fileinfo->getExtension()) == 'gpx') {
        $filename = $fileinfo->getFilename();

        echo "Processing $filename ";

        $travel = new Travel('batch/'.$filename);
        $tracks = $travel->extract();

        foreach($tracks as $track) {
            DB::startTransaction();

            $insert = DB::insert(
                'tracks',
                array(
                    'name'       => $track->name,
                    'timestamp'  => date('Y-m-d H:i:s', $track->date),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => null
                )
            );

            if ($insert) {
                $id = DB::insertId();
                DB::delete('points', "track_id=%i", $id);
                foreach($track->segments as $point) {
                    DB::insert(
                        'points',
                        array(
                            'track_id'  => $id,
                            'latitude'  => $point->latitude,
                            'longitude' => $point->longitude,
                            'elevation' => $point->elevation,
                            'timestamp' => date('Y-m-d H:i:s', $point->timestamp)
                        )
                    );
                }

                DB::commit();
                echo "[DONE] <br/>";
                $processed[] = $filename;
            } else {
                DB::rollback();
                echo "[FAIL]<br/>";
            }
        }
    }
}

?>
Batch done. Total processed <?=count($processed);?> files. <br/>
<table>
    <tr>
        <th>No</th>
        <th>Filename</th>
    </tr>
<?php
$no = 1;
foreach($processed as $filename) {
    echo '<tr><td>'.$no++.'</td><td>'.$filename.'</td></tr>';
}
?>
</table>