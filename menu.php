<nav class="top-bar" data-topbar>
    <ul class="title-area">
        <li class="name">
            <h1><a href="#">GPX Analyzer</a></h1>
        </li>
    </ul>

    <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="right">
            <li><a href="#">Copyright &copy; 2014 - imamabudaud@gmail.com</a></li>
        </ul>

        <!-- Left Nav Section -->
        <ul class="left">
            <li><a href="upload.php">Upload GPX</a></li>
            <li><a href="index.php">List</a></li>
            <li><a href="route.php">Route</a></li>
        </ul>
    </section>
</nav>
