<?php

include_once('library/database.php');
if (isset($_GET['id']) && $_GET['id'] != ""){
    $id = $_GET['id'];
} else {
    die("It's something..");
}

$track  = DB::queryFirstRow('SELECT * FROM tracks WHERE id=%i', $id);
$points = DB::query('SELECT * FROM points WHERE track_id=%i', $id);
$totalTime = DB::queryOneField('total_time', 'SELECT (MAX(timestamp) - MIN(timestamp)) as total_time FROM points WHERE track_id=%i', $id);
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>

    <style>
        .map {
            position: relative;
            width: 460px;
            height: 460px;
            padding-top: 15px;
        }
    </style>

    <script src="foundation-5.2.2/js/vendor/jquery.js"></script>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
    <script src="foundation-5.2.2/js/foundation.min.js"></script>

    <script src="js/gmap.cluster.distance.js"></script>
    <script src="js/moment.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

    <script>
        function initialize() {
            var points = [
                <?php foreach($points as $point) { ?>
                { latitude: <?=$point['latitude'];?>, longitude: <?=$point['longitude'];?>, timestamp: <?=strtotime($point['timestamp']);?> },
                <?php } ?>
            ];

            var mapOptions = {
                zoom: 13,
                center: new google.maps.LatLng( points[0]['latitude'], points[0]['longitude'] ),
                mapTypeId: google.maps.MapTypeId.MAP
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var travelCoordinates = [];
            for(var i=0; i<points.length; i++) {
                travelCoordinates.push(new google.maps.LatLng(points[i].latitude, points[i].longitude));
            }
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < travelCoordinates.length; i++) {
                bounds.extend(travelCoordinates[i]);
            }
            map.fitBounds(bounds);

            var flightPath = new google.maps.Polyline({
                path: travelCoordinates,
                geodesic: true,
                strokeColor: '#FF6961',
                strokeOpacity: 0.8,
                strokeWeight: 2
            });
            flightPath.setMap(map);

            var markerA = new google.maps.Marker({
                    position: new google.maps.LatLng(points[0].latitude, points[0].longitude),
                    map: map,
                    icon: 'image/markerA.png',
                    draggable: true,
                    metadata : {
                        timestamp: points[0].timestamp,
                        closestIndex: 0
                    }
                }),
                markerB = new google.maps.Marker({
                    position: new google.maps.LatLng(points[points.length - 1].latitude, points[points.length - 1].longitude),
                    map: map,
                    icon: 'image/markerB.png',
                    draggable: true,
                    metadata : {
                        timestamp: points[points.length - 1].timestamp,
                        closestIndex: points.length - 1
                    }
                }),
                liveDot = new google.maps.Marker({
                    position: null,
                    map: map,
                    icon: 'image/dot.png',
                    draggable: true
                });


            // Make the redDot follow the movement of marker (closest path marker-path)
            google.maps.event.addListener(markerA, 'drag', function(event){
                snapMarkerToPath(liveDot, event);
            });

            google.maps.event.addListener(markerB, 'drag', function(event){
                snapMarkerToPath(liveDot, event);
            });

            // Snap the marker after dragend to closest path location (the latest redDot position)
            google.maps.event.addListener(markerA, 'dragend', function(event){
                markerA.setPosition(liveDot.getPosition());
                markerA.metadata = liveDot.metadata;
                liveDot.setPosition(null);
                colorizePath();
                updateStatistics();
            });

            google.maps.event.addListener(markerB, 'dragend', function(event){
                markerB.setPosition(liveDot.getPosition());
                markerB.metadata = liveDot.metadata;
                liveDot.setPosition(null);
                colorizePath();
                updateStatistics();
            });

            var coloredPath = new google.maps.Polyline({
                path: [],
                geodesic: true,
                strokeColor: '#99FF33',
                strokeOpacity: 0.8,
                strokeWeight: 4
            });
            coloredPath.setMap(map);

            function colorizePath() {
                var start = Math.min(markerA.metadata.closestIndex, markerB.metadata.closestIndex),
                    end   = Math.max(markerA.metadata.closestIndex, markerB.metadata.closestIndex),
                    colorizedPoints = [];

                for (var i=start; i<=end; i++) {
                    colorizedPoints.push(new google.maps.LatLng(points[i].latitude, points[i].longitude));
                }

                coloredPath.setPath(colorizedPoints);
            }

            function snapMarkerToPath(marker, event) {
                var closestMarkerPosition = getClosestMarkerPosition(event.latLng.lat(), event.latLng.lng());

                marker.setPosition(new google.maps.LatLng( closestMarkerPosition.latitude, closestMarkerPosition.longitude));
                marker.metadata = closestMarkerPosition.metadata;
            }

            function getClosestMarkerPosition(markerLatitude, markerLongitude) {
                var distances = points.map(function(pathPoint) {
                        return {
                            point : {
                                latitude  : pathPoint.latitude,
                                longitude : pathPoint.longitude,
                                timestamp : pathPoint.timestamp
                            },
                            distance : getDistanceFromLatLonInKm(pathPoint.latitude, pathPoint.longitude, markerLatitude, markerLongitude)
                        };
                    }),
                    closestIndex = 0; // mark point index which closest to marker

                var min = { distance: Infinity, point: null };
                for (var i=0; i<distances.length; i++) {
                    if (distances[i].distance < min.distance) {
                        min = distances[i];
                        closestIndex = i;
                    }
                }

                return {
                    latitude : min.point.latitude,
                    longitude: min.point.longitude,
                    metadata: {
                        timestamp    : min.point.timestamp,
                        closestIndex : closestIndex
                    }
                }
            }

            // Analyze the markers (A-B)
            function updateStatistics() {
                $('#marker-a-position').html(markerA.getPosition().lat().toFixed(5) + ', ' + markerA.getPosition().lng().toFixed(5));
                $('#marker-b-position').html(markerB.getPosition().lat().toFixed(5) + ', ' + markerB.getPosition().lng().toFixed(5));

                var timeA = moment.unix(markerA.metadata.timestamp),
                    timeB = moment.unix(markerB.metadata.timestamp);

                $('#marker-a-timestamp').html(timeA.format("dddd, MMMM Do YYYY, h:mm:ss a"));
                $('#marker-b-timestamp').html(timeB.format("dddd, MMMM Do YYYY, h:mm:ss a"));

                // Calculate duration
                var duration = moment.duration(timeB.diff(timeA));
                $('#track-duration').html(duration.hours() + ' hours ' + duration.minutes() + ' min ' + duration.seconds() + ' s');

                // Calculate distances
                var distance = sumDistance(markerA.metadata.closestIndex, markerB.metadata.closestIndex);
                $('#track-distance').html(distance.toFixed(2) + ' km');

                // Total points travelled
                var totalPoints = Math.abs(markerA.metadata.closestIndex - markerB.metadata.closestIndex);
                $('#track-points').html(totalPoints);

                // Fancy
                $('.result').fadeOut(0).fadeIn(500);

                // Compare map data
                compare.pointA.latitude  = markerA.getPosition().lat();
                compare.pointA.longitude = markerA.getPosition().lng();
                compare.pointA.timestamp = markerA.metadata.timestamp;
                compare.pointA.index     = markerA.metadata.closestIndex;
                compare.pointB.latitude  = markerB.getPosition().lat();
                compare.pointB.longitude = markerB.getPosition().lng();
                compare.pointB.timestamp = markerB.metadata.timestamp;
                compare.pointB.index     = markerB.metadata.closestIndex;
                console.log(compare);
            }

            updateStatistics();

            function sumDistance(startIndex, endIndex) {
                var sum = 0,
                    a = Math.min(startIndex, endIndex),
                    b = Math.max(startIndex, endIndex);

                for (var i=a; i<b; i++) {
                    sum += getDistanceFromLatLonInKm(points[i].latitude, points[i].longitude,points[i+1].latitude, points[i+1].longitude);
                }

                return sum;
            }
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        var compare = {
            mapId : <?=$id;?>,
            pointA : {
                latitude: null,
                longitude: null,
                timestamp: null,
                index: null
            },
            pointB : {
                latitude: null,
                longitude: null,
                timestamp: null,
                index: null
            }
        };

        $(document).ready(function(){
            $('#btn-compare').on('click', function(){
                window.location.href =
                    'compare.php?id=' + compare.mapId +
                    '&a-latitude='  + compare.pointA.latitude +
                    '&a-longitude=' + compare.pointA.longitude +
                    '&a-timestamp=' + compare.pointA.timestamp +
                    '&a-index='     + compare.pointA.index +
                    '&b-latitude='  + compare.pointB.latitude +
                    '&b-longitude=' + compare.pointB.longitude +
                    '&b-timestamp=' + compare.pointB.timestamp +
                    '&b-index='     + compare.pointB.index;
            });
        })
    </script>
</head>
<body>

<?php include('menu.php'); ?>

<section class="body">
    <div class="row">
        <div class="large-6 columns">
            <div class="map">
                <div id='map-canvas'></div>
            </div>
        </div>
        <div class="large-6 columns result">
            <h2>Travel Information</h2>
            <table>
                <tr>
                    <th>A position</th>
                    <td>:</td>
                    <td id="marker-a-position"></td>
                </tr>
                <tr>
                    <th>B position</th>
                    <td>:</td>
                    <td id="marker-b-position"></td>
                </tr>
                <tr>
                    <th>A timestamp</th>
                    <td>:</td>
                    <td id="marker-a-timestamp"></td>
                </tr>
                <tr>
                    <th>B timestamp</th>
                    <td>:</td>
                    <td id="marker-b-timestamp"></td>
                </tr>
            </table>
            <h2>Results</h2>
            <table>
                <tr>
                    <th>Distance</th>
                    <td>:</td>
                    <td id="track-distance"></td>
                </tr>
                <tr>
                    <th>Duration</th>
                    <td>:</td>
                    <td id="track-duration"></td>
                </tr>
                <tr>
                    <th>Total Point</th>
                    <td>:</td>
                    <td id="track-points"></td>
                </tr>
            </table>
            <a href="#" id="btn-compare" class="button expand">Compare with Others</a>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){

    });
</script>
</body>
</html>