<?php

include_once('library/track.class.php');
include_once('library/database.php');

if (!isset($_GET['session']) || !is_file('serialized/'.$_GET['session'].'.ser')) {
    die('Its something...');
}

$serializedFile = 'serialized/'.$_GET['session'].'.ser';
$tracks = unserialize(file_get_contents($serializedFile));

foreach($tracks as $track) {
    DB::startTransaction();
    
    $insert = DB::insert(
        'tracks',
        array(
            'name'       => $track->name,
            'timestamp'  => date('Y-m-d H:i:s', $track->date),
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => null
        )
    );
    
    if ($insert) {
        $id = DB::insertId();
        DB::delete('points', "track_id=%i", $id);
        foreach($track->segments as $point) {
            DB::insert(
                'points',
                array(
                    'track_id'  => $id,
                    'latitude'  => $point->latitude,
                    'longitude' => $point->longitude,
                    'elevation' => $point->elevation,
                    'timestamp' => date('Y-m-d H:i:s', $point->timestamp)
                )
            );
        }
        
        DB::commit();
    } else {
        DB::rollback();
        die;
    }
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
</head>
<body>

<?php include('menu.php'); ?>

<section class="body">
    <div class="panel callout radius">
        <h5>Tracks successfully saved to database.</h5>
        <p>Now you can view and manage your tracks <a href="manage.php">Here</a>.</p>
    </div>
</section>
</body>
</html>