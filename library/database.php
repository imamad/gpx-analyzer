<?php

include_once ('meekrodb.2.2.class.php');

DB::$user     = 'root';
DB::$password = 'password';
DB::$dbName   = 'gpx';
DB::$host     = 'localhost'; //defaults to localhost if omitted
DB::$port     = '3306'; // defaults to 3306 if omitted
DB::$encoding = 'utf8'; // defaults to latin1 if omitted

// source: http://stackoverflow.com/questions/8599200/calculate-distance-given-2-points-latitude-and-longitude
function getQueryIntersection($aLatitude, $aLongitude, $bLatitude, $bLongitude) {
    $threshold = 0.1; // km
    return "
    SELECT DISTINCT track_id FROM (
        SELECT *,(((acos(sin((".$aLatitude."*pi()/180)) *
                    sin((points.latitude*pi()/180))+cos((".$aLatitude."*pi()/180)) *
                    cos((points.latitude*pi()/180)) * cos(((".$aLongitude."-points.longitude )*
                    pi()/180))))*180/pi())*60*1.1515*1.609344
                ) as distance
                FROM points
                HAVING distance <= ".$threshold."
    ) pointsA
    WHERE track_id IN (
        SELECT DISTINCT track_id FROM (
            SELECT *,(((acos(sin((".$bLatitude."*pi()/180)) *
                        sin((points.latitude*pi()/180))+cos((".$bLatitude."*pi()/180)) *
                        cos((points.latitude*pi()/180)) * cos(((".$bLongitude."-points.longitude )*
                        pi()/180))))*180/pi())*60*1.1515*1.609344
                    ) as distance
                    FROM points
                    HAVING distance <= ".$threshold."
        ) pointsB
    )";
}

function calculateTravelDuration($trackId, $aLatitude, $aLongitude, $bLatitude, $bLongitude) {
    $queryA = "
    SELECT UNIX_TIMESTAMP(timestamp) as timestamp,(((acos(sin((".$aLatitude."*pi()/180)) *
                sin((points.latitude*pi()/180))+cos((".$aLatitude."*pi()/180)) *
                cos((points.latitude*pi()/180)) * cos(((".$aLongitude."-points.longitude )*
                pi()/180))))*180/pi())*60*1.1515*1.609344
            ) as distance
            FROM points
            WHERE track_id = ".$trackId."
            HAVING distance <= 0.1
            ORDER BY distance
            LIMIT 1
    ";

    $queryB = "
    SELECT UNIX_TIMESTAMP(timestamp) as timestamp,(((acos(sin((".$bLatitude."*pi()/180)) *
                sin((points.latitude*pi()/180))+cos((".$bLatitude."*pi()/180)) *
                cos((points.latitude*pi()/180)) * cos(((".$bLongitude."-points.longitude )*
                pi()/180))))*180/pi())*60*1.1515*1.609344
            ) as distance
            FROM points
            WHERE track_id = ".$trackId."
            HAVING distance <= 0.1
            ORDER BY distance
            LIMIT 1
    ";

    $aRow = DB::queryFirstRow($queryA);
    $bRow = DB::queryFirstRow($queryB);

    return abs($aRow['timestamp'] - $bRow['timestamp']);
}