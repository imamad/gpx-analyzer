<?php
include 'distance.class.php';

class Point {
    public $latitude;
    public $longitude;
    public $elevation;
    public $timestamp;
    
    public function Point($latitude, $longitude, $elevation, $timestamp) {
        $this->latitude  = $latitude;   
        $this->longitude = $longitude;   
        $this->elevation = $elevation;   
        $this->timestamp = $timestamp;   
    }
}

class Track {
	public $name;
	public $date;
	public $segments;
}

class Travel {
	private $xml;
    public $tracks;
    public $points;
    
    public function Travel($filename) {
        if ($filename != '') {
            $this->loadFile($filename);   
        }
    }

	public function loadFile($filename) {
		$this->xml = simplexml_load_file($filename);
		if ($this->xml){
			return true;
		}else{
			$this->xml = false;
			return false;
		}
	}
    
    private function stringToTimestamp($string) {
        $dateInfo = date_parse_from_format("j M Y h:i a", $string); // eg.  9 Mei 2014  6:50 pm
        
        return mktime(
            $dateInfo['hour'], $dateInfo['minute'], $dateInfo['second'],
            $dateInfo['month'], $dateInfo['day'], $dateInfo['year']
        );  
    }

    // @return Track
	public function extract() {
		$this->tracks = [];
		
		foreach ($this->xml->trk as $trk){
			$track  = new Track();
			$track->name = (string) $trk->name;
            $track->date = $this->stringToTimestamp((string) $trk->desc);

			// There may be multiple segments if GPS connectivity was lost - process each seperately
			foreach ($trk->trkseg as $trkseg){
				$x=0;
				foreach ($trkseg->trkpt as $trkpt) {
					$attributes = $trkpt->attributes();

					$track->segments[] = new Point(
                            doubleval($attributes->lat), 
						    doubleval($attributes->lon), 
                            doubleval($trkpt->ele), 
                            strtotime($trkpt->time)
					);	

					// Up the counters
					$x++;
				}
				
			}


			$this->tracks[] = $track;
		}

		return $this->tracks;
	}
    
    public function getPoints() {
         foreach($this->tracks as $track) {
            foreach($track->segments as $point) {
                $this->points[] = $point;
            }
         }
        
        return $this->points;
    }
}

class TravelStatistic {
    private $tracks;
    
    public function TravelStatistic($tracks) {
        $this->tracks = $tracks;
    }
    
    public function countTrack() {
        return count($this->tracks);   
    }
    
    public function countPoint() {
        $segments = 0;
        foreach($this->tracks as $track) {
            $segments += count($track->segments);
        }
        
        return $segments;
    }
    
    public function totalTime() {
        $total = 0;
        foreach($this->tracks as $track) {
            $total += ($track->segments[count($track->segments)-1]->timestamp - $track->segments[0]->timestamp);
        }

        return $total;
    }
}

// Test
//$travel = new Travel('../tracks/1410409831.gpx');
//$tracks = $travel->extract();
//$duration = new TravelStatistic($tracks);
//
//echo $duration->totalTime();