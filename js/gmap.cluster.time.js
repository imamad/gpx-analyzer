function cluster(points, threshold) {
    var travel = 0;
    var clustered = [];
    // first point
    clustered.push({
        latitude: points[0].latitude,
        longitude: points[0].longitude,
        timestamp: points[0].timestamp,
        index: 0
    });

    for (var i=1; i<points.length; i++) {
        var timeDistance = distance(points[i-1].timestamp, points[i].timestamp);
        var nodeDistance = getDistanceFromLatLonInKm(
            points[i-1].latitude, points[i-1].longitude,
            points[i].latitude, points[i].longitude
        );
        var clusteredNodeDistance = distance(clustered[clustered.length-1].timestamp, points[i].timestamp);

        if (clusteredNodeDistance > threshold) {
            clustered.push({
                latitude: points[i].latitude,
                longitude: points[i].longitude,
                timestamp: points[i].timestamp,
                index: i,
                travel: travel
            });

            travel = 0;
        } else {
            travel += nodeDistance;
        }
    }

    return doClassify(clustered);
}

function doClassify(clustered) {
    var travels = [];
    for(var i=1; i<clustered.length; i++) {
        travels.push(clustered[i].travel);
    }

    var clusters = kmeans(travels, 3);

    for(var i=0; i<clustered.length; i++) {
        for(var c=0; c<clusters.length; c++) {
            if (clusters[c].indexOf(clustered[i].travel) != -1) {
                clustered[i].cluster = c;
                break;
            }
        }
    }

    return clustered;
}

function distance(t1, t2) {
    return Math.abs(t2-t1) / 60; // in minute
}

// http://stackoverflow.com/questions/27928/how-do-i-calculate-distance-between-two-latitude-longitude-points
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

// http://www.mymessedupmind.co.uk/index.php/javascript-k-mean-algorithm
function kmeans(arrayToProcess, numberOfCluster) {
    var groups = new Array();
    var centroids = new Array();
    var oldCentroids = new Array();
    var changed = false;

    // order the input array
    arrayToProcess.sort(function (a, b) {
        return a - b
    });

    // initialise group arrays
    for (initGroups = 0; initGroups < numberOfCluster; initGroups++) {
        groups[initGroups] = new Array();
    }

    // pick initial centroids
    initialCentroids = Math.round(arrayToProcess.length / (numberOfCluster + 1));
    for (i = 0; i < numberOfCluster; i++) {
        centroids[i] = arrayToProcess[ (initialCentroids * (i + 1)) ];
    }

    do
    {
        for (j = 0; j < numberOfCluster; j++) {
            groups[j] = [];
        }

        changed = false;
        for (i = 0; i < arrayToProcess.length; i++) {
            Distance = -1;
            oldDistance = -1

            for (j = 0; j < numberOfCluster; j++) {
                distance = Math.abs(centroids[j] - arrayToProcess[i]);

                if (oldDistance == -1) {
                    oldDistance = distance;
                    newGroup = j;
                }
                else if (distance <= oldDistance) {
                    newGroup = j;
                    oldDistance = distance;
                }
            }
            groups[newGroup].push(arrayToProcess[i]);
        }

        oldCentroids = centroids;

        for (j = 0; j < numberOfCluster; j++) {
            total = 0;
            newCentroid = 0;

            for (i = 0; i < groups[j].length; i++) {
                total += groups[j][i];
            }
            newCentroid = total / groups[newGroup].length;
            centroids[j] = newCentroid;
        }

        for (j = 0; j < numberOfCluster; j++) {
            if (centroids[j] != oldCentroids[j]) {
                changed = true;
            }
        }
    }
    while (changed == true);

    return groups;
}