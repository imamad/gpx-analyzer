function cluster(latLonArray, threshold) { 
    if (!threshold) threshold = 1; // km
    var clustered = [];

    // first point
    clustered.push(latLonArray[0]);
    for (var i=1; i<latLonArray.length; i++) {
        var latestClustered = clustered[clustered.length-1];
        var currentLatLon   = latLonArray[i];

        if (getDistanceFromLatLonInKm(
                latestClustered.latitude, latestClustered.longitude, 
                currentLatLon.latitude,   currentLatLon.longitude) 
            > threshold
        ) {
            clustered.push(latLonArray[i]);    
        }
    }

    return clustered;
}

// http://stackoverflow.com/questions/27928/how-do-i-calculate-distance-between-two-latitude-longitude-points
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}