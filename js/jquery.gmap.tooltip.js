function MapTooltip(map, tip, onBeforeShow, onBeforeHide) {
    // Don't forget the tip
    var $tooltip = tip;
    // One timer
    var timer = 0;
    // Don't loose our scope
    var self = this;

    if(!$tooltip.hasClass('tooltip-enabled')) {
        // Mouse enter
        $tooltip.bind('mouseenter.tooltip', function(e) {
            stopTimer();
        });

        // Mouse leave
        $tooltip.bind('mouseleave.tooltip', function(e) {
            startTimer(function() { self.hide(200) }, 100);
        });

        $tooltip.addClass('tooltip-enabled');
    }

    function startTimer(callback, time) {
        stopTimer();
        timer = setTimeout(callback, time);
    }

    function stopTimer() {
        clearTimeout(timer);
    }

    this.show = function(marker, event) {
        // Hide
        self.hide(0);

        $tooltip.data('MapTooltips.marker', marker);

        // Callback
        onBeforeShow(marker, $tooltip);

        var offset = $('#map-canvas').offset();
        $tooltip.css({
            position: 'absolute',
            'z-index': 100,
            top: event.pixel.y + 33, left: event.pixel.x + 5
        });

        $tooltip.animate({"left": "+=10px", "opacity": "toggle"}, 100);
    };

    this.hide = function(time) {
        var marker = $tooltip.data('MapTooltips.marker');

        if(marker && typeof onBeforeHide == 'function') {
            onBeforeHide(marker, $tooltip);
        }
        $tooltip.fadeOut(time);
    };

    this.addMarker = function(marker) {
        google.maps.event.addListener(marker, 'mouseover', function(event) {
            startTimer(function(){ self.show(marker, event) }, 100);
        });

        google.maps.event.addListener(marker, 'mouseout', function(event) {
            startTimer(function() { self.hide(200) }, 500);
        });
    };
};