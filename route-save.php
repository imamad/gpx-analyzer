<?php
include_once('library/database.php');

header('Content-Type: application/json');

if (isset($_POST['route'])) {
    $route = $_POST['route'];
    if (
       !isset($route['waypoints']) || $route['waypoints'] == ''
    || !isset($route['pointA']['latitude'])  || $route['pointA']['latitude'] == ''
    || !isset($route['pointA']['longitude']) || $route['pointA']['longitude'] == ''
    || !isset($route['pointB']['latitude'])  || $route['pointB']['latitude'] == ''
    || !isset($route['pointB']['longitude']) || $route['pointB']['longitude'] == ''
    ) {
        http_response_code(400);
        echo 'Bad input format';
        die;
    }

    $id    = null;

    if (isset($_POST['id']) && $_POST['id'] != '') {
        $id = $_POST['id'];

        DB::update(
            'routes',
            array(
                'updated_at'     => date('Y-m-d H:i:s', time()),
                'startLatitude'  => $route['pointA']['latitude'],
                'startLongitude' => $route['pointA']['longitude'],
                'endLatitude'    => $route['pointB']['latitude'],
                'endLongitude'   => $route['pointB']['longitude'],
                'waypoints'      => serializeWaypoints($route['waypoints'])
            ),
            'id=%d', $id
        );
    } else {
        DB::insert(
            'routes',
            array(
                'created_at' => date('Y-m-d H:i:s', time()),
                'startLatitude'  => $route['pointA']['latitude'],
                'startLongitude' => $route['pointA']['longitude'],
                'endLatitude'    => $route['pointB']['latitude'],
                'endLongitude'   => $route['pointB']['longitude'],
                'waypoints'      => serializeWaypoints($route['waypoints'])
            )
        );

        $id = DB::insertId();
    }

    $route['id'] = $id;
    echo json_encode($route);
}

function serializeWaypoints($waypoints) {
    return join('|', array_map(function($point){
        return $point[0].','.$point[1];
    }, $waypoints));
}