<?php

if (   !isset($_GET['id'])
    || !isset($_GET['a-latitude'])
    || !isset($_GET['a-longitude'])
    || !isset($_GET['a-timestamp'])
    || !isset($_GET['b-latitude'])
    || !isset($_GET['b-longitude'])
    || !isset($_GET['b-timestamp'])
) {
    die ('Parameter are : id, a-latitude, a-longitude, a-timestamp, b-latitude, b-longitude, b-timestamp');
}

include_once('library/database.php');


$id         = $_GET['id'];
$aLatitude  = $_GET['a-latitude'];
$aLongitude = $_GET['a-longitude'];
$aTimestamp = $_GET['a-timestamp'];
$aIndex     = $_GET['a-index'];
$bLatitude  = $_GET['b-latitude'];
$bLongitude = $_GET['b-longitude'];
$bTimestamp = $_GET['b-timestamp'];
$bIndex     = $_GET['b-index'];

$track  = DB::queryFirstRow('SELECT * FROM tracks WHERE id=%i', $id);
$points = DB::query('SELECT * FROM points WHERE track_id=%i', $id);
$matchingTracks = DB::queryFirstColumn(getQueryIntersection($aLatitude, $aLongitude, $bLatitude, $bLongitude));

$durations = [];
$sum       = 0;
foreach($matchingTracks as $m) {
    $d           = calculateTravelDuration($m, $aLatitude, $aLongitude, $bLatitude, $bLongitude);
    $durations[] = ["track" => $m, "seconds" => $d, "duration" => gmdate("H:i:s", $d)];
    $sum        += $d;
}
$average = $sum/count($durations);
// @todo: display map based on requested lat lon bound
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>
    <script src="foundation-5.2.2/js/vendor/jquery.js"></script>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
    <script src="foundation-5.2.2/js/foundation.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

    <script>
        google.maps.Polyline.prototype.simplifyLine = function(tolerance){
            var res = null;

            if(this.getPath() && this.getPath().getLength()){
                var points = this.getPath().getArray();

                var Line = function( p1, p2 ) {
                    this.p1 = p1;
                    this.p2 = p2;

                    this.distanceToPoint = function( point ) {
                        // slope
                        var m = ( this.p2.lat() - this.p1.lat() ) / ( this.p2.lng() - this.p1.lng() ),
                        // y offset
                            b = this.p1.lat() - ( m * this.p1.lng() ),
                            d = [];
                        // distance to the linear equation
                        d.push( Math.abs( point.lat() - ( m * point.lng() ) - b ) / Math.sqrt( Math.pow( m, 2 ) + 1 ) );
                        // distance to p1
                        d.push( Math.sqrt( Math.pow( ( point.lng() - this.p1.lng() ), 2 ) + Math.pow( ( point.lat() - this.p1.lat() ), 2 ) ) );
                        // distance to p2
                        d.push( Math.sqrt( Math.pow( ( point.lng() - this.p2.lng() ), 2 ) + Math.pow( ( point.lat() - this.p2.lat() ), 2 ) ) );
                        // return the smallest distance
                        return d.sort( function( a, b ) {
                            return ( a - b ); //causes an array to be sorted numerically and ascending
                        } )[0];
                    };
                };

                var douglasPeucker = function( points, tolerance ) {
                    if ( points.length <= 2 ) {
                        return [points[0]];
                    }
                    var returnPoints = [],
                    // make line from start to end
                        line = new Line( points[0], points[points.length - 1] ),
                    // find the largest distance from intermediate poitns to this line
                        maxDistance = 0,
                        maxDistanceIndex = 0,
                        p;
                    for( var i = 1; i <= points.length - 2; i++ ) {
                        var distance = line.distanceToPoint( points[ i ] );
                        if( distance > maxDistance ) {
                            maxDistance = distance;
                            maxDistanceIndex = i;
                        }
                    }
                    // check if the max distance is greater than our tollerance allows
                    if ( maxDistance >= tolerance ) {
                        p = points[maxDistanceIndex];
                        line.distanceToPoint( p, true );
                        // include this point in the output
                        returnPoints = returnPoints.concat( douglasPeucker( points.slice( 0, maxDistanceIndex + 1 ), tolerance ) );
                        // returnPoints.push( points[maxDistanceIndex] );
                        returnPoints = returnPoints.concat( douglasPeucker( points.slice( maxDistanceIndex, points.length ), tolerance ) );
                    } else {
                        // ditching this point
                        p = points[maxDistanceIndex];
                        line.distanceToPoint( p, true );
                        returnPoints = [points[0]];
                    }
                    return returnPoints;
                };
                res = douglasPeucker( points, tolerance );
                // always have to push the very last point on so it doesn't get left off
                res.push( points[points.length - 1 ] );
            }
            return res;
        };

        function initialize() {
            var points = [
                <?php
                for($i=min($aIndex, $bIndex); $i<=max($aIndex, $bIndex); $i++) {
                    $point = $points[$i];
                    echo "{
                            latitude  : ".$point['latitude'].",
                            longitude : ".$point['longitude'].",
                            timestamp : ".strtotime($point['timestamp'])."
                          },";
                }
                ?>
            ];
            var mapOptions = {
                zoom: 13,
                center: new google.maps.LatLng( points[0]['latitude'], points[0]['longitude'] ),
                mapTypeId: google.maps.MapTypeId.MAP
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var travelCoordinates = [];
            for(var i=0; i<points.length; i++) {
                travelCoordinates.push(new google.maps.LatLng(points[i].latitude, points[i].longitude));
            }
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < travelCoordinates.length; i++) {
                bounds.extend(travelCoordinates[i]);
            }
            map.fitBounds(bounds);

            var trackPath = new google.maps.Polyline({
                path: travelCoordinates,
                geodesic: true,
                strokeColor: '#FF6961',
                strokeOpacity: 0.8,
                strokeWeight: 2
            });
            var simplifiedLinePath = trackPath.simplifyLine(0.0005);
            var simplifiedLine = new google.maps.Polyline({
                map: map,
                path: simplifiedLinePath,
                geodesic: true,
                strokeColor: '#FF6961',
                strokeOpacity: 0.8,
                strokeWeight: 2
            });

            var markerA = new google.maps.Marker({
                    position: new google.maps.LatLng(points[0].latitude, points[0].longitude),
                    map: map,
                    icon: 'image/markerA.png',
                    draggable: true,
                    metadata : {
                        timestamp: points[0].timestamp,
                        closestIndex: 0
                    }
                }),
                markerB = new google.maps.Marker({
                    position: new google.maps.LatLng(points[points.length - 1].latitude, points[points.length - 1].longitude),
                    map: map,
                    icon: 'image/markerB.png',
                    draggable: true,
                    metadata : {
                        timestamp: points[points.length - 1].timestamp,
                        closestIndex: points.length - 1
                    }
                });
        }
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
</head>
<body>

<?php include('menu.php'); ?>

<section class="body">
    <div class="row">
        <div class="large-6 columns">
            <div style="width: 460px; height: 460px; padding-top: 15px;">
                <div id='map-canvas'></div>
            </div>
        </div>
        <div class="large-6 columns">
            <h2>Travel Statistics</h2>
            <table>
                <tr>
                    <th>Track name</th>
                    <td>:</td>
                    <td><?=$track['name'];?></td>
                </tr>
                <tr>
                    <th>Travel Date</th>
                    <td>:</td>
                    <td><?=$track['timestamp'];?></td>
                </tr>
                <tr>
                    <th>Average Time</th>
                    <td>:</td>
                    <td><?=gmdate("H:i:s", $average);?></td>
                </tr>
                <tr>
                    <th>Total Track</th>
                    <td>:</td>
                    <td><?=count($durations);?></td>
                </tr>
            </table>
        </div>
    </div>
</section>

</body>
</html>