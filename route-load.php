<?php
include_once('library/database.php');
if (isset($_GET['id']) && $_GET['id'] != ""){
    $id     = $_GET['id'];
    $route  = DB::queryFirstRow('SELECT * FROM routes WHERE id=%i', $id);

    $route['waypoints'] = deserializeWaypoints($route['waypoints']);

    header('Content-Type: application/json');
    echo json_encode($route);
}

function deserializeWaypoints($waypoints) {
    return array_map(function($point) {
            return explode(',', $point);
        },
        explode('|', $waypoints));
}
?>