<?php
if (isset($_GET['id']) && $_GET['id'] != "") {
    include_once('library/database.php');

    DB::startTransaction();
    DB::delete('points', 'track_id=%i', $_GET['id']);
    DB::delete('tracks', 'id=%i', $_GET['id']);
    DB::commit();

    header('location:manage.php');
    die;
}