<?php
include_once('library/database.php');

$result = DB::query('
    SELECT
		tracks.id,
		tracks.name,
		tracks.timestamp,
		COUNT(*) as total_point,
		(MAX(points.timestamp) - MIN(points.timestamp)) as seconds
    FROM
        tracks
    INNER JOIN points
        ON tracks.id = points.track_id
    GROUP BY tracks.id
    ORDER BY tracks.name
   ');
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
</head>
<body>

<?php include('menu.php'); ?>

<section class="body">
    <h2>Manage tracks</h2>
    <table>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Date</th>
            <th>Total Point</th>
            <th>Time Spent</th>
            <th></th>
        </tr>

        <?php
        foreach($result as $row) {
        ?>
        <tr>
            <td><?=$row['id'];?></td>
            <td><?=$row['name'];?></td>
            <td><?=$row['timestamp'];?></td>
            <td><?=$row['total_point'];?></td>
            <td><?=gmdate('H:i:s', $row['seconds']);?></td>
            <td>
                <a href="view.php?id=<?=$row['id'];?>" class="tiny button">View</a>
                <a href="analyze.php?id=<?=$row['id'];?>" class="tiny button">Analyze</a>
                <a href="delete.php?id=<?=$row['id'];?>" class="tiny button alert">Delete</a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</section>
</body>
</html>