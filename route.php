<?php
if (isset($_GET['id']) && $_GET['id'] != ""){
    $id = $_GET['id'];
} else {
    $id = null;
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="foundation-5.2.2/css/foundation.css" />
    <link rel="stylesheet" href="css/app.css"/>
    <script src="foundation-5.2.2/js/vendor/jquery.js"></script>
    <script src="foundation-5.2.2/js/vendor/modernizr.js"></script>
    <script src="foundation-5.2.2/js/foundation.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

    <script>
        var map,
            service  = new google.maps.DirectionsService(),
            renderer = new google.maps.DirectionsRenderer( {'draggable':true} ),
            pointA   = null,
            pointB   = null;

        function initialize() {
            var myOptions = {
                zoom: 11,
                center: new google.maps.LatLng(-6.600000, 106.800000),
                mapTypeId: google.maps.MapTypeId.HYBRID,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                draggableCursor: "crosshair",
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_RIGHT
                }
            }

            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
            renderer.setMap(map);

            google.maps.event.addListener(map, "click", function(evt) {
                if (pointA == null) {
                    pointA = (evt.latLng);
                } else {
                    pointB = evt.latLng;
                    service.route({
                        origin: pointA,
                        destination: pointB,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING,
                        avoidTolls: true
                    }, function(result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            $('#route-distance').html( result.routes[0].legs[0].distance.text );
                            $('#route-duration').html( result.routes[0].legs[0].duration.text );

                            renderer.setDirections(result);
                        }
                    });
                }
            });

            <?php
            if ($id) {
                echo "loadRoute();";
            }
            ?>
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        function saveRoute() {
            console.log(renderer);
            var route = renderer.directions.routes[0].legs[0],
                data  = {
                    pointA    : null,
                    pointB    : null,
                    waypoints : []
                };

            data.pointA = { 'latitude': route.start_location.lat(), 'longitude':route.start_location.lng() };
            data.pointB = { 'latitude': route.end_location.lat(),   'longitude':route.end_location.lng()   };

            for(var i=0;i<route.via_waypoints.length;i++) {
                data.waypoints[i] = [route.via_waypoints[i].lat(),route.via_waypoints[i].lng()]
            }

            $.post( "route-save.php", { route: data, id: '<?=$id;?>' } )
                .done(function(response) {
                    console.log('Saving route: ');
                    console.log(response);
                    alert('Route saved');
                });
        }

        function loadRoute() {
            $.get( "route-load.php?id=<?=$id;?>" )
                .done(function(response){
                    console.log('loading route');
                    console.log(response);
                    var wp = [];
                    for(var i=0; i<response.waypoints.length; i++) {
                        wp[i] = {
                            'location' : new google.maps.LatLng(response.waypoints[i][0], response.waypoints[i][1]),
                            'stopover' : false
                        }
                    }

                    service.route(
                        {
                            origin      : new google.maps.LatLng(response.startLatitude, response.startLongitude),
                            destination : new google.maps.LatLng(response.endLatitude,   response.endLongitude),
                            waypoints   : wp,
                            travelMode  : google.maps.DirectionsTravelMode.DRIVING,
                            avoidTolls  : true
                        },
                        function(result,status) {
                            if(status == google.maps.DirectionsStatus.OK) {
                                $('#route-distance').html( result.routes[0].legs[0].distance.text );
                                $('#route-duration').html( result.routes[0].legs[0].duration.text );

                                renderer.setDirections(result);
                            }
                        }
                    );
                });
        }

        $(document).ready(function(){
            $('#btn-save').on('click', function() {
                console.log("saving waypoints and route :");
                saveRoute();
            });
        });
    </script>

</head>
<body>

<?php include('menu.php'); ?>

<div id='map-canvas'></div>

<section class="body">
    <br/>
    <div class="row">
        <div class="large-6 columns">
            <table>
                <tr>
                    <th>Distance</th>
                    <td>:</td>
                    <td id="route-distance"></td>
                </tr>
                <tr>
                    <th>Duration</th>
                    <td>:</td>
                    <td id="route-duration"></td>
                </tr>
            </table>
        </div>
        <div class="large-6 columns">
        <?php if ($id) { ?>
            <a href="#" id="btn-save" class="button expand">Update Route</a>
        <?php } else { ?>
            <a href="#" id="btn-save" class="button expand">Save Route</a>
        <?php } ?>
        </div>
    </div>
</section>

</body>
</html>