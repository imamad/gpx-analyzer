
<?php
include 'library/distance.class.php';

class Track {
	public $name;
	public $date;
	public $segments;
}

class Point {
	private $xml;

	public function loadFile($filename) {
		$this->xml = simplexml_load_file($filename);
		if ($this->xml){
			return true;
		}else{
			$this->xml = false;
			return false;
		}
	}

	public function extract() {
		$tracks = [];
		
		foreach ($this->xml->trk as $trk){
			$track  = new Track();
			$track->name = (string) $trk->name;
			$track->date = (string) $trk->desc;

			// There may be multiple segments if GPS connectivity was lost - process each seperately
			foreach ($trk->trkseg as $trkseg){


				// Trackpoint details in trk - Push them into our object
				$x=0;
				foreach ($trkseg->trkpt as $trkpt){
					$key = "trackpt$x";

					$attributes = $trkpt->attributes();

					$lat  = doubleval($attributes->lat);
					$lon  = doubleval($attributes->lon);
					$ele  = doubleval($trkpt->ele);
					$time      = date('Y-m-d H:i:s', strtotime($trkpt->time));
					$timestamp = strtotime($trkpt->time);

					$track->segments["point_$x"] = [
						'lat' => $lat, 
						'lon' => $lon, 
						'ele' => $ele, 
						'timestamp' => $timestamp,
						'time' => $time
					];

					if ($x > 0) {
						$track->segments["point_$x"]['d'] = Distance::calculate(
							$track->segments["point_$x"]['lat'], $track->segments["point_$x"]['lon'],
							$track->segments["point_".($x-1)]['lat'], $track->segments["point_".($x-1)]['lon'],
							'K'
						) * 1000;

						$deltaTime = $track->segments["point_$x"]['timestamp'] - $track->segments["point_".($x-1)]['timestamp']; // seconds 
						if ($deltaTime > 0) {
							$track->segments["point_$x"]['v'] = $track->segments["point_$x"]['d'] / $deltaTime;
						} else {
							$track->segments["point_$x"]['v'] = 0;
						}						
					}

					// Up the counters
					$x++;
				}
				
			}


			$tracks[] = $track;
		}

		return $tracks;
	}
}


$gpx = new Point();
$gpx->loadFile('data/track.gpx');
print '<pre>';
print_r($gpx->extract());
?>